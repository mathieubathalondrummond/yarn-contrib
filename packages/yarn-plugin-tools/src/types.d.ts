/* eslint-disable @typescript-eslint/prefer-namespace-keyword,no-var */
declare module NodeJS {
  interface Global {
    YARN_VERSION?: string
  }
}

declare var YARN_VERSION: string | undefined
